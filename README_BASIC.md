# VR.Hercules (For Basic Usage)

This file contains the user documentation.
Note that there is also [developer documentation](README_DEVELOPERS.md).

## Interpreter

To get Python, follow the [official installation instructions](https://wiki.python.org/moin/BeginnersGuide/Download).

## Virtual Environment

This package is best installed into a virtual environment.
To set up such a virtual environment:

```bash
python3.10 -m venv venv
```

## Installation

To install this package into your virtual environment:

```bash
venv/bin/python -m pip install vr.hercules
```

## Configuration

Create a `config.yaml` file with these contents:

```yaml
Config:
    greeting: Hello
    greetee: World
```

## Parsing

Create a `config.py` file with the following contents:

```python
from vr_hercules.parser.config_parsing import parse_config


class Config:
    greeting: str
    greetee: str


Config: type = parse_config(
    config_file_path='config.yaml',
)
```

## Autocompletion

Your IDE can now automatically complete the fields of your configuration.
In PyCharm, that looks like this:

![Image showing autocompletion in action](_images/autocompletion.png)

## Runtime Configuration

Since the configuration is just a class, you can set its fields at runtime:

```python
Config.greeting = 'Howdy'
```

> Note: This is useful for iterating configuration values like so:
> ```python
> for greeting in ('Hello', 'Howdy'):
>     Config.greeting = greeting
> ```

## Printing Configurations

This package includes functionality for printing configurations classes:

```python
>>> print_config(config=Config)
Config:
    greetee: World
    greeting: Hello
```

## Using Configurations

```python
>>> from config import Config
>>> print(f'{Config.greeting}, {Config.greetee}!')
Hello, World!
```

## Custom Types

Defining custom types is possible, but more involved.
See the [advanced readme](README_ADVANCED.md) for details.
