# VR.Hercules (For Advanced Usage)

In addition to the [README.md](README.md),
    this file documents how to use VR.Hercules with custom types.

## Custom Types

On its own, VR.Hercules supports handling all Python built-in types.
If you want to handle custom types, you need extend the parsing as follows:

Configure your application in `config_with_punctuation.yaml`:

```yaml
Config:
    greeting: Hello
    greetee: World
    punctuation: PunctuationEnum.ExclamationMark
```

Then you can define a custom type and config type, parse the built-in fields,
parse your custom field and use it just like a built-in one:

```python
from enum import Enum
from pathlib import Path

from vr_hercules.hello_world.config import Config
from vr_hercules.parser.config_parsing import parse_config
from vr_hercules.parser.enumeration_parsing import parse_enum


# Define a custom attribute type:
class PunctuationEnum(Enum):
    Dot: int = 0
    ExclamationMark: int = 1
    QuestionMark: int = 2


# Extend the config type:
class ConfigPunctuation(Config):
    punctuation: str


# Parse built-in fields:
ConfigPunctuation: type = parse_config(  # type: ignore # noqa
    config_file_path=Path('config_with_punctuation.yaml'),
)

# Parse custom field:
parse_enum(
    config=ConfigPunctuation,
    attribute_name='punctuation',
    enum=PunctuationEnum,
)

# Use your custom fields just like built-in fields:
if ConfigPunctuation.punctuation == PunctuationEnum.ExclamationMark:
    print(f'{ConfigPunctuation.greeting}, {ConfigPunctuation.greetee}!')
```

## Parsing Enums from Nested Configurations

This can even be done with nested attributes.
Just pass nest your configuration file, as you like e.g.:

```yaml
Config:
    greeting: Hello
    greetee: World
    SubConfig:
        punctuation: PunctuationEnum.ExclamationMark
```

Then, pass the path to the enumeration as the `attribute_path` parameter.
In this case:

```python
parse_enum(
    config=ConfigPunctuation,
    attribute_path=['SubConfig'],
    attribute_name='punctuation',
    enum=PunctuationEnum,
)
```
