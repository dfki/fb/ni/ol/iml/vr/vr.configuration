from enum import Enum


class PunctuationEnum(Enum):
    Dot: int = 0
    ExclamationMark: int = 1
    QuestionMark: int = 2
